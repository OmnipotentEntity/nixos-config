{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    nixpkgs-release.url = "github:nixos/nixpkgs/release-22.05";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, nixpkgs-release, nixpkgs-unstable, ... }:
  let 
    system = "x86_64-linux";

    overlays = [ 
      (final: prev: {
        release = import nixpkgs-release {
          inherit system;
          # overlays = self.overlays; # .${system};
          config = { allowUnfree = true; };
        };
        unstable = import nixpkgs-unstable {
          inherit system;
          # overlays = self.overlays; # .${system};
          config = { allowUnfree = true; };
        };
      })
    ];

    pkgs = import nixpkgs {
      inherit system;
      inherit overlays;
      config = {
        allowUnfree = true;
      };
    };

  in {
    nixosConfigurations.abraxas = nixpkgs.lib.nixosSystem {
      inherit system;
      modules = [ 
        { 
          nixpkgs = {
            inherit overlays;
            config.allowUnfree = true;
          };
        }

        {
          _module.args.release = import nixpkgs-release {
            system = "x86_64-linux";
            config = { allowUnfree = true; };
          };
        }

        {
          _module.args.unstable = import nixpkgs-unstable {
            system = "x86_64-linux";
            config = { allowUnfree = true; };
          };
        }

        ./configuration.nix
      ];
    };
  };
}
