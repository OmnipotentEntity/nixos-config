# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, unstable, release, lib, ... }:

let
  nuhorizonCert = ''
    -----BEGIN CERTIFICATE-----
    Redacted
    -----END CERTIFICATE-----
  '';
in
{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    autoOptimiseStore = true;
    daemonCPUSchedPolicy = "idle";
    daemonIOSchedClass = "idle";
    gc = {
      automatic = true;
      options = "--delete-older-than 30d";
      dates = "weekly";
    };
  };

  boot.loader = {
    systemd-boot.enable = true;
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/efi";
    };

    grub = {
      efiSupport = true;
      device = "/dev/disk/by-uuid/CE65-F99D";
    };
  };

  fileSystems."/home" = {
      device = "/dev/disk/by-uuid/9307c990-e121-467e-b57b-28e3ee7c0e30";
      fsType = "ext4";
    };

  networking.hostName = "abraxas"; # Define your hostname.
  networking.networkmanager.enable = true;
  networking.dhcpcd.extraConfig = ''
    static domain_name_servers=192.168.1.1
  '';
  # networking.extraHosts = ''
  #     10.1.10.200 registry.nuhorizontech.com
  #     127.0.0.1 nht.registry.local
  #   '';

  services.dnsmasq = {
    enable = true;
    servers = [
      "1.1.1.1"
      "8.8.8.8"
      "8.8.4.4"
      "4.4.4.4"
      "4.4.4.3"
    ];
    # extraConfig = ''
    #   address=/registry.nuhorizontech.com/10.1.10.200
    # '';
  };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "America/Chicago";

  fonts.fonts = with pkgs; [
    carlito
    corefonts
    dina-font
    fira-code
    fira-code-symbols
    liberation_ttf
    # mplus-outline-fonts
    nerdfonts
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    proggyfonts
    redhat-official-fonts
    ubuntu_font_family
  ];

  environment.wordlist = {
    enable = true;
    lists = {
      WORDLIST = [ "${pkgs.scowl}/share/dict/words.txt" ];
      AUGMENTED_WORDLIST = [
      "${pkgs.scowl}/share/dict/words.txt"
      "${pkgs.scowl}/share/dict/words.variants.txt"
      (builtins.toFile "extra-words" ''
      desynchronization
      oobleck  '')
      ];
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    ark
    aspell
    aspellDicts.en
    aspellDicts.en-computers
    aspellDicts.en-science
    astyle
    autoconf
    automake
    bc
    binutils
    bison
    calibre
    cmakeCurses
    cppcheck
    ctags
    dhcpcd
    direnv
    dnsutils
    dogecoin
    egl-wayland
    file
    firefox
    gcc
    gdbm
    gimp
    git
    glibc
    gnumake
    gnupg
    htop
    inetutils
    kdeconnect
    killall
    kwayland-integration
    libreoffice
    libtool
    libyaml
    lynx
    ncurses
    nix-prefetch-scripts
    nload
    nodejs
    okular
    openssl
    p7zip
    pciutils
    pkgconfig
    procps
    python
    python3
    qemu_full
    readline
    ripgrep
    root
    ruby
    scowl
    slack
    spectacle
    sqlite
    stdenv
    steam
    sysstat
    texlive.combined.scheme-full
    thunderbird
    tmux
    tree
    unzip
    usbutils
    vimHugeX
    wget
    wineWowPackages.stable
    xwayland
    yakuake
    zip
    zlib
    zsh
    (pkgs.writeShellScriptBin "nixFlakes" ''
    exec ${pkgs.nixUnstable}/bin/nix --experimental-features "nix-command flakes" "$@"
    '')
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  programs.zsh.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.forwardX11 = true;
  services.fail2ban.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    5432 # postgresql
  ]; 
  networking.firewall.allowedTCPPortRanges = [
    { # kde connect
      from = 1714;
      to = 1764;
    }
  ];
  networking.firewall.allowedUDPPorts = [
    53 #dns
  ]; 
  networking.firewall.allowedUDPPortRanges = [ 
    { # kde connect
      from = 1714;
      to = 1764;
    }
  ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # services.openvpn.servers = {
  #   nht = {
  #     config = '' config /root/nixos/openvpn/client2.conf '';
  #     up = "/run/current-system/sw/bin/dhcpcd tap0 -G";
  #   };
  # };

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplip ];

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.support32Bit = true;

  hardware.cpu.intel.updateMicrocode = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us";
  services.xserver.xkbOptions = "eurosign:e";

  # Enable third monitor
  #services.xserver.screenSection = ''
  #  Monitor        "Monitor0"
  #  DefaultDepth    24
  #  Option         "Stereo" "0"
  #  Option         "nvidiaXineramaInfoOrder" "DFP-3"
  #  Option         "metamodes" "DP-2: nvidia-auto-select +0+0, DP-0: nvidia-auto-select +5120+0, DP-4: nvidia-auto-select +2560+0"
  #  Option         "SLI" "Off"
  #  Option         "MultiGPU" "Off"
  #  Option         "BaseMosaic" "off"
  #  SubSection     "Display"
  #      Depth       24
  #  EndSubSection
  #'';

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Enable plasma wayland
  services.xserver.displayManager.sessionPackages = [
    (pkgs.plasma-workspace.overrideAttrs
      (old: { passthru.providedSessions = [ "plasmawayland" ]; }))
  ];

  # Postgres
  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_12;
    dataDir = "/home/postgresql";
    enableTCPIP = true;
    authentication = lib.mkForce ''
    # TYPE  DATABASE        USER            ADDRESS                 METHOD
    local   all             all                                     trust
    host    all             all             127.0.0.1/32            trust
    host    all             all             ::1/128                 trust
    host    all             all             0.0.0.0/0               scram-sha-256
    host    all             all             ::0/0                   scram-sha-256
    '';
    settings = {
      password_encryption = "scram-sha-256";
      max_wal_senders = "10";
      max_replication_slots = "10";
      wal_level = "replica";
      hot_standby = "on";
      archive_mode = "on";
      archive_command = "/run/current-system/sw/bin/true";
    };
  };

  # nuhorizon ssl cert
  # security.pki.certificates = [
  #   nuhorizonCert
  # ];

  # environment.etc."docker/certs.d/registry.nuhorizontech.com/ca.crt" = {
  #   group = "docker";
  #   mode = "0640";
  #   text = nuhorizonCert;
  # };

  # environment.etc."docker/daemon.json" = {
  #   group = "docker";
  #   mode = "0640";
  #   text = ''
  #     {
  #       "insecure-registries": ["registry.nuhorizontech.com"]
  #     }
  #   '';
  # };

  # Nvidia Drivers
  nixpkgs.config.allowUnfree = true;
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.stable;
  # hardware.nvidia.modesetting.enabled = true;
  hardware.opengl.driSupport32Bit = true;

  # environment.etc."egl/egl_external_platform.d/10_nvidia_wayland.json".text = ''
  #     {
  #       "file_format_version" : "1.0.0",
  #       "ICD" : {
  #           "library_path" : "${pkgs.egl-wayland}/lib/libnvidia-egl-wayland.so"
  #       }
  #   }
  # '';

  systemd.services.nvidia-control-devices = {
    wantedBy = [ "multi-user.target" ];
    serviceConfig.ExecStart = "${pkgs.linuxPackages.nvidia_x11.bin}/bin/nvidia-smi";
  };

  # systemd.services.docker-cc-persist = {
  #   after = [ "docker.service" ];
  #   requires = [ "docker.service" ];
  #   wantedBy = [ "multi-user.target" ];
  #   serviceConfig = {
  #     Type = "oneshot";
  #     ExecStart = "${pkgs.docker}/bin/docker start nht_registry_local";
  #   };
  # };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.omnipotententity = {
    isNormalUser = true;
    uid = 1000;
    hashedPassword = "Redacted";
    # extraGroups = [ "wheel" "networkmanager" "docker" ];
    extraGroups = [ "wheel" "networkmanager" ];
    createHome = true;
    description = "Michael Reilly";
    group = "users";
    home = "/home/omnipotententity";
    packages = with pkgs; [
      amarok
      audacity
      chromium
      release.discord
      docker
      dropbox
      element-desktop
      ghc
      hexchat
      hunspellDicts.en-us-large
      imagemagickBig
      kdenlive
      keepassx2
      ktorrent
      mathematica
      mplayer
      obs-studio
      oh-my-zsh
      plasma-browser-integration
      python38Packages.flake8
      tdesktop
      unrar
      valgrind
      vlc
      x11vnc
      zoom-us
    ];
    shell = pkgs.zsh;
  };

  # users.users.kyren = {
  #   isNormalUser = true;
  #   uid = 1001;
  #   hashedPassword = "Redacted";
  #   extraGroups = [ "wheel" "networkmanager" ];
  #   createHome = true;
  #   description = "Catherine West";
  #   group = "users";
  #   home = "/home/kyren";
  #   packages = with pkgs; [ ];
  #   shell = pkgs.zsh;
  # };

  users.users.root = {
    hashedPassword = "Redacted";
  };

  users.mutableUsers = false;

  virtualisation.virtualbox.host.enable = true;
  virtualisation.virtualbox.host.enableExtensionPack = true;

  # virtualisation.docker.enable = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "22.05"; # Did you read the comment?

}
